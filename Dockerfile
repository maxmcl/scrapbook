FROM ubuntu:20.04
SHELL ["/bin/bash", "-c"]
RUN apt update -y && apt install -y build-essential curl && \
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh \
    | bash && source /root/.nvm/nvm.sh && nvm install 17.0.1

ENV PATH /root/.nvm/versions/node/v17.0.1/bin:$PATH

WORKDIR /app
COPY package-lock.json package.json ./
RUN npm ci
COPY . .
