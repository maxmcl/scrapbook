import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export class Product {
  name: string;
  description: string;
  price: number;
  imgUrl: string;
  id: number;

  constructor(
    name: string,
    description: string,
    price: number,
    imgUrl: string,
    id: number
  ) {
    this.name = name;
    this.description = description;
    this.price = price;
    this.imgUrl = imgUrl;
    this.id = id;
  }

  toString(): string {
    return `Product [id: ${this.id}]`;
  }
}

export class CartProduct extends Product {
  quantity: number;

  constructor(product: Product) {
    super(
      product.name,
      product.description,
      product.price,
      product.imgUrl,
      product.id
    );
    this.quantity = 1;
  }

  getProductPrice(): number {
    return this.quantity * this.price;
  }

  toString(): string {
    return `CartProduct [id: ${this.id} | quantity: ${this.quantity}]`;
  }
}

export interface Snackbar {
  show: boolean;
  variant: string;
  message: string;
}

export interface State {
  products: Array<Product>;
  cart: Array<CartProduct>;
  snackbar: Snackbar;
}

export interface Getters {
  cartTotal: number;
}

export default new Vuex.Store<State>({
  state: {
    products: [
      {
        name: "Apple",
        description: "A green apple",
        price: 0.55,
        imgUrl: require("@/assets/green-apple.jpg"),
        id: 0,
      },
      {
        name: "Banana",
        description: "Non radioactive banana",
        price: 0.2,
        imgUrl: require("@/assets/banana.jpg"),
        id: 1,
      },
      {
        name: "Grapefruit",
        description: "Most hated grapefruit",
        price: 0.77,
        imgUrl: require("@/assets/grapefruit.jpg"),
        id: 2,
      },
      {
        name: "Pineapple",
        description: "Spiky pineapple",
        price: 3,
        imgUrl: require("@/assets/pineapple.jpg"),
        id: 3,
      },
      {
        name: "Kiwi box",
        description: "Hairy kiwis",
        price: 5.5,
        imgUrl: require("@/assets/kiwi.jpg"),
        id: 4,
      },
    ],
    cart: [],
    snackbar: {
      show: false,
      variant: "primary",
      message: "Item added to cart",
    },
  },
  mutations: {
    addItemToCart(state: State, product: Product): void {
      const cartProduct = state.cart.find(
        (cartProduct: Product) => product.id === cartProduct.id
      );
      if (cartProduct) {
        console.log(`Product already found, updating quantity by 1`);
        cartProduct.quantity += 1;
      } else {
        console.log(`Adding ${product} to cart`);
        state.cart.push(new CartProduct(product));
      }
    },
    updateCartQuantity(
      state: State,
      { cartProduct, quantity }: { cartProduct: CartProduct; quantity: number }
    ): void {
      const index = state.cart.indexOf(cartProduct);
      if (index > -1) {
        console.log(`Product in cart, updating quantity by ${quantity}`);
        cartProduct.quantity += quantity;
        if (cartProduct.quantity <= 0) {
          console.log("Product has <= 0 quantity, removing");
          state.cart.splice(index, 1);
        }
      } else {
        console.log(`${cartProduct} not found in cart???`);
      }
    },
    updateSnackbar(state, { show }: { show: boolean }): void {
      state.snackbar["show"] = show;
    },
    removeFromCart(state: State, cartProduct: CartProduct): void {
      const index = state.cart.indexOf(cartProduct);
      if (index > -1) {
        console.log(`Removing ${cartProduct}`);
        state.cart.splice(index, 1);
      } else {
        console.log(`${cartProduct} not found in cart???`);
      }
    },
  },
  getters: {
    cartTotal(state: State): number {
      const sum = (total: number, cartProduct: CartProduct) =>
        total + cartProduct.getProductPrice();
      return state.cart.reduce(sum, 0);
    },
  },
});
