import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";

import Home from "@/views/Home.vue";
import Cart from "@/views/Cart.vue";
import Store from "@/views/Store.vue";
import Checkout from "@/views/Checkout.vue";
import ThankYou from "@/views/ThankYou.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  { path: "/", name: "home", component: Home },
  { path: "/cart", name: "cart", component: Cart },
  { path: "/store", name: "store", component: Store },
  { path: "/checkout", name: "checkout", component: Checkout },
  { path: "/thankyou", name: "thankyou", component: ThankYou },
];

const router = new VueRouter({
  routes,
});

export default router;
