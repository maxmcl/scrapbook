export type UserData = {
  email: string;
  name: string;
  phone: string;
  street: string;
  state: string;
  zip: string;
};

export type UserDataRules = Record<string, (value: string) => boolean | string>;

export type Data = {
  step: number;
  checkoutForm: boolean;
  data: UserData;
  rules: UserDataRules;
};
